﻿using System;
using Unity.Entities;
using System.Collections.Generic;

namespace Assets.Scripts.ExtensionMethods
{
    public static class SystemExtensions
    {
        public static void AddSystemsToGroup(World world, ComponentSystemGroup group, IEnumerable<Type> systemTypes)
        {
            if (world == null || group == null)
                return;

            foreach (var systemType in systemTypes)
            {
                var system = world.GetOrCreateSystem(systemType);
                group.AddSystemToUpdateList(system);
            }
        }
    }
}
