using UnityEngine;
using Unity.Entities;
using UnityEngine.UIElements;

public class PauseMenuController : MonoBehaviour
{
    private World _world;
    private VisualElement _rootVisualElement;
    private Button _resumeButton;

    void Start()
    {
        _world = World.DefaultGameObjectInjectionWorld;
        _rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        _resumeButton = _rootVisualElement.Q<Button>("resume-btn");

        _resumeButton.clicked += OnResumeButtonClick;

        ShowOrHidePauseMenu(false);
    }

    private void OnResumeButtonClick()
    {
        ShowOrHidePauseMenu(false);
    }

    private void ShowOrHidePauseMenu(bool state)
    {
        _rootVisualElement.SetEnabled(state);
        _rootVisualElement.visible = state;
        _world.QuitUpdate = state;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ShowOrHidePauseMenu(!_rootVisualElement.enabledSelf);

        if (!_rootVisualElement.enabledSelf)
            return;
    }
}
