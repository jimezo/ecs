﻿using System.Linq;
using UnityEngine;
using Unity.Entities;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;


namespace Assets
{
    public class AssetLoader : MonoBehaviour
    {
        [SerializeField] private List<AssetLabelReference> _labels;
        private readonly List<GameObject> _gameObjects = new List<GameObject>();
        private BlobAssetStore _blobAssetStore;
        private static EntityManager _entityManager;

        private async void Start()
        {
            _blobAssetStore = new BlobAssetStore();
            _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

            //foreach (var assetLabel in _labels)
            //{
            //    await LoadAssetAddToList(assetLabel.labelString, _gameObjects);
            //}

            //foreach (var gameObj in _gameObjects)
            //{
            //    GameObjectConversionUtility.ConvertGameObjectHierarchy(gameObj,
            //        GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, _blobAssetStore));
            //}

            SetCannonComponent();
        }

        private void OnDestroy()
        {
            _blobAssetStore.Dispose();
        }

        public static async Task LoadAssetAddToList(string label, List<GameObject> completedObjects)
        {
            var handle = Addressables.LoadAssetAsync<GameObject>(label).Task;
            await handle;
            completedObjects.Add(handle.Result);
        }

        private static void SetCannonComponent()
        {
            var cannonPrefab = _entityManager.GetAllEntities()
                .FirstOrDefault((e) => _entityManager.HasComponent(e, ComponentType.ReadOnly(typeof(CannonTag))));

            if (cannonPrefab == Entity.Null)
                return;

            var cannon = cannonPrefab;

            var bullet = _entityManager.GetAllEntities()
                .FirstOrDefault((e) => _entityManager.HasComponent(e, ComponentType.ReadOnly(typeof(BulletTag))));

            if (bullet == Entity.Null)
                return;

            _entityManager.AddComponentData(cannon, new CannonComponent
            {
                BulletPrefab = bullet
            });
        }
    }
}
