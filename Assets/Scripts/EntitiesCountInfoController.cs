using UnityEngine;
using Unity.Entities;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;

public class EntitiesCountInfoController : MonoBehaviour
{
    private World _world;
    private VisualElement _rootVisualElement;
    private Label _entityCountLabel;
    private Label _bulletsCountLabel;

    private EntityCountChangeEventSystem _entityCountChangeEventSystem;
    private BulletCountChangeEventSystem _bulletCountChangeEventSystem;

    void Start()
    {
        _world = World.DefaultGameObjectInjectionWorld;

        _entityCountChangeEventSystem = _world.GetOrCreateSystem<EntityCountChangeEventSystem>();
        _bulletCountChangeEventSystem = _world.GetOrCreateSystem<BulletCountChangeEventSystem>();

        _rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        _entityCountLabel = _rootVisualElement.Q<Label>("entity-count");
        _bulletsCountLabel = _rootVisualElement.Q<Label>("bullets-count");

        _entityCountChangeEventSystem.OnNonPrefabEntityCount += OnNonPrefabEntityCount;
        _bulletCountChangeEventSystem.OnBulletEntityCount += OnBulletCountChanged;

        ShowOrHideEntitiesCountInfo(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
            ShowOrHideEntitiesCountInfo(!_rootVisualElement.enabledSelf);
    }

    private void ShowOrHideEntitiesCountInfo(bool state)
    {
        _rootVisualElement.SetEnabled(state);
        _rootVisualElement.visible = state;
    }

    private void OnBulletCountChanged(int count)
    {
        if (!_rootVisualElement.enabledSelf)
            return;

        _bulletsCountLabel.text = count.ToString();
    }

    private void OnNonPrefabEntityCount(int count)
    {
        if (!_rootVisualElement.enabledSelf)
            return;

        _entityCountLabel.text = count.ToString();
    }
}
