using Assets;
using Unity.Entities;

public sealed class WeaponSwapSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem _endSimulationEcbSystem;

    protected override void OnCreate()
    {
        _endSimulationEcbSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var ecb = _endSimulationEcbSystem.CreateCommandBuffer();

        Entities
            .ForEach((ref WeaponsComponent weapons, in InputDataComponent input) => {

                if (!input.IsAlpha1 && !input.IsAlpha2 && !input.IsAlpha3)
                    return;

                if (weapons.CurrentWeapon != Entity.Null)
                    ecb.RemoveComponent<WeaponTriggerTag>(weapons.CurrentWeapon);

                var weapon = Entity.Null;

                if (input.IsAlpha1)
                    weapon = weapons.FirstWeapon;

                else if (input.IsAlpha2)
                    weapon = weapons.SecondWeapon;

                else if (input.IsAlpha3)
                    weapon = weapons.ThirdWeapon;

                ecb.AddComponent<WeaponTriggerTag>(weapon);
                weapons.CurrentWeapon = weapon;

            }).Schedule();

        _endSimulationEcbSystem.AddJobHandleForProducer(Dependency);
    }
}
