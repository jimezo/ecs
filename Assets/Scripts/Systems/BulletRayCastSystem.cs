﻿using Assets;
using Unity.Jobs;
using Unity.Burst;
using Unity.Physics;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Physics.Systems;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateAfter(typeof(StepPhysicsWorld))]
public sealed class BulletRayCastSystem : SystemBase
{
    private BuildPhysicsWorld _buildPhysicsWorld;
    private EndFixedStepSimulationEntityCommandBufferSystem _entityCommandBufferSystem;

    protected override void OnCreate()
    {
        _buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        _entityCommandBufferSystem = World.GetOrCreateSystem<EndFixedStepSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var collisionWorld = _buildPhysicsWorld.PhysicsWorld.CollisionWorld;
        var ecb = _entityCommandBufferSystem.CreateCommandBuffer();

        Dependency = Entities
            .WithAll<BulletTag>()
            .ForEach((Entity entity,
                in RaycastComponent raycastComponent,
                in Translation translation,
                in Rotation rotation,
                in BulletInitialDataComponent bulletInitialData,
                in EntityPrefabReferenceComponent prefabReference) =>
        {
            var input = new RaycastInput
            {
                Start = translation.Value,
                End = translation.Value + math.forward(rotation.Value) * raycastComponent.Distance,
                Filter = CollisionFilter.Default
            };

            if (!collisionWorld.CastRay(input, out var hit))
                return;

            //TODO : There should be something to do with the bullet

            var collisionStateComponent = new BulletCollisionStateComponent
            {
                BulletPrefab = prefabReference.Prefab,
                InitialVelocity = bulletInitialData.InitialVelocity,
                Translation = translation,
                Rotation = rotation,
                RaycastHit = hit
            };

            ecb.AddComponent(entity, collisionStateComponent);
            ecb.DestroyEntity(entity);
            ecb.AddSharedComponent(entity, new BulletStateSharedComponent { State = EBulletCollisionState.Ricochet });
            ecb.RemoveComponent<RaycastComponent>(entity);

        }).WithBurst().Schedule(Dependency);

        _entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
