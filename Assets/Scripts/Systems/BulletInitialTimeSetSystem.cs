using Unity.Jobs;
using Unity.Entities;

[DisableAutoCreation]
[UpdateBefore(typeof(BulletDirectionSystem))]
public class BulletInitialTimeSetSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var elapsedTime = (float)Time.ElapsedTime;

        Entities.ForEach((ref BulletInitialDataComponent bulletInitialData) => {

            if (bulletInitialData.InitialTime == 0 && elapsedTime >= 0)
                bulletInitialData.InitialTime = elapsedTime;

        }).Schedule();
    }
}
