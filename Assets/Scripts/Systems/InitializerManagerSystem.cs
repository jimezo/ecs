﻿using Unity.Entities;

namespace Assets
{
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public sealed class InitializerManagerSystem : SystemBase
    {
        private BulletsCollection _bulletsCollection;
        private RocketsCollection _rocketsCollection;

        private BulletsFactory _bulletsFactory;

        protected override void OnCreate()
        {
            _bulletsFactory = new BulletsFactory(World, _bulletsCollection, _rocketsCollection);

            BulletsPool = new BulletsPool(_bulletsFactory.CreateBullet);
            RocketsPool = new RocketsPool(_bulletsFactory.CreateRocket);
        }

        public IBulletsPool BulletsPool { get; private set; }
        public IBulletsPool RocketsPool { get; private set; }

        protected override void OnUpdate()
        {
            
        }
    }
}

