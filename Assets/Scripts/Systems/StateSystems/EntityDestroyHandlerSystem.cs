using Assets;
using Unity.Jobs;
using Unity.Entities;

[UpdateBefore(typeof(EndSimulationEntityCommandBufferSystem))]
public class EntityDestroyHandlerSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem _commandBufferSystem;

    protected override void OnCreate()
    {
        base.OnCreate();
        _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var ecb = _commandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.ForEach((Entity entity, int entityInQueryIndex, in EntityDestroySystemStateComponent destroySystemStateComponent) => {

            ecb.RemoveComponent<EntityDestroySystemStateComponent>(entityInQueryIndex, entity);

        }).ScheduleParallel();

        _commandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
