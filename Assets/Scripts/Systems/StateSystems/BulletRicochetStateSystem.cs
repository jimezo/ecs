using Assets;
using Unity.Jobs;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

[UpdateAfter(typeof(BulletRayCastSystem))]
public class BulletRicochetStateSystem : BulletStateSystemBase
{
    protected override EBulletCollisionState FilterState => EBulletCollisionState.Ricochet;

    protected override void OnUpdate()
    {
        var elapsedTime = (float) Time.ElapsedTime;
        var ecb = EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities
            .WithSharedComponentFilter(BulletStateFilter)
            .ForEach((Entity entity, int entityInQueryIndex, in BulletCollisionStateComponent collisionStateComponent) =>
        {
            ecb.RemoveComponent<BulletStateSharedComponent>(entityInQueryIndex, entity);
            ecb.RemoveComponent<BulletCollisionStateComponent>(entityInQueryIndex, entity);

            var normalInitialVelocity = math.normalize(collisionStateComponent.InitialVelocity);
            var reflectedDirection = math.reflect(normalInitialVelocity, collisionStateComponent.RaycastHit.SurfaceNormal);
            var reflectedNormalDirection = math.normalize(reflectedDirection);
            var newEntity = ecb.Instantiate(entityInQueryIndex, collisionStateComponent.BulletPrefab);
            var bulletRotation = quaternion.LookRotationSafe(reflectedNormalDirection, math.right());

            var rotationComponent = new Rotation { Value = bulletRotation };
            var translationComponent = new Translation { Value = math.forward(bulletRotation) };
            var prefabRefComponent = new EntityPrefabReferenceComponent { Prefab = collisionStateComponent.BulletPrefab };
            var bulletInitDataComponent = new BulletInitialDataComponent
            {
                InitialPosition = collisionStateComponent.RaycastHit.Position,
                InitialVelocity = reflectedNormalDirection * 100,
                InitialTime = elapsedTime,
                GravityFactor = 1f
            };

            ecb.SetComponent(entityInQueryIndex, newEntity, rotationComponent);
            ecb.SetComponent(entityInQueryIndex, newEntity, translationComponent);
            ecb.AddComponent(entityInQueryIndex, newEntity, prefabRefComponent);
            ecb.AddComponent(entityInQueryIndex,newEntity, bulletInitDataComponent);

        }).WithBurst().ScheduleParallel();

        EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
