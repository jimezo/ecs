using Assets;
using Unity.Jobs;
using Unity.Entities;

public class EntityDestoryStateSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem _entityCommandBufferSystem;

    protected override void OnCreate()
    {
        _entityCommandBufferSystem = World.GetExistingSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var ecb = _entityCommandBufferSystem.CreateCommandBuffer();

        Entities.ForEach((Entity entity, ref EntityDestroySystemStateComponent entityDestoryStateSystemComponent) => {

            if (!entityDestoryStateSystemComponent.IsDestroyed)
            {
                ecb.DestroyEntity(entity);
                entityDestoryStateSystemComponent.IsDestroyed = true;
            }

        }).Schedule();

        _entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
