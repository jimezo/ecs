using Assets;
using Unity.Jobs;
using Unity.Entities;

[UpdateAfter(typeof(BulletRayCastSystem))]
public class BulletPenetrationStateSystem : BulletStateSystemBase
{
    protected override EBulletCollisionState FilterState => EBulletCollisionState.Penetration;

    protected override void OnUpdate()
    {
        var ecb = EntityCommandBufferSystem.CreateCommandBuffer();

        Entities
            .WithSharedComponentFilter(BulletStateFilter)
            .ForEach((Entity entity, in BulletCollisionStateComponent collisionStateComponent) => 
        {
            ecb.RemoveComponent<BulletStateSharedComponent>(entity);
            ecb.RemoveComponent<BulletCollisionStateComponent>(entity);

        }).WithBurst().Schedule();

        EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}
