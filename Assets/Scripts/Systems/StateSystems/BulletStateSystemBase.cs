﻿using Unity.Entities;

public abstract class BulletStateSystemBase : SystemBase
{
    protected BeginSimulationEntityCommandBufferSystem EntityCommandBufferSystem;
    protected BulletStateSharedComponent BulletStateFilter;

    protected abstract EBulletCollisionState FilterState { get; }

    protected override void OnCreate()
    {
        base.OnCreate();
        EntityCommandBufferSystem = World.GetExistingSystem<BeginSimulationEntityCommandBufferSystem>();
        BulletStateFilter = new BulletStateSharedComponent { State = FilterState };
    }
}
