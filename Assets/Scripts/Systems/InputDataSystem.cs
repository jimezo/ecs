﻿using UnityEngine;
using Unity.Entities;

public sealed class InputDataSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");

        var isJump = Input.GetKeyDown(KeyCode.Space);

        var alpha1 = Input.GetKeyDown(KeyCode.Alpha1);
        var alpha2 = Input.GetKeyDown(KeyCode.Alpha2);
        var alpha3 = Input.GetKeyDown(KeyCode.Alpha3);

        var isRKeyDown = Input.GetKeyDown(KeyCode.R);

        Entities.ForEach((ref InputDataComponent inputComponent) =>
        {
            inputComponent.InputH = x;
            inputComponent.InputV = y;
            inputComponent.IsRKeyDown = isRKeyDown;

            inputComponent.IsJumpInput = isJump;

            inputComponent.IsAlpha1 = alpha1;
            inputComponent.IsAlpha2 = alpha2;
            inputComponent.IsAlpha3 = alpha3;
        }).ScheduleParallel();
    }
}
