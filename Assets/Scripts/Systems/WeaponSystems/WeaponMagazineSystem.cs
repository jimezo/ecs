﻿using Assets;
using Unity.Entities;

public sealed class WeaponMagazineSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities
            .WithAll<WeaponTriggerTag>()
            .ForEach((ref MagazineComponent magazine, in InputDataComponent keyboardInput) =>
        {
            magazine.IsEmpty = magazine.BulletCount == 0;

            if (keyboardInput.IsRKeyDown && magazine.BulletCount != magazine.MaxBulletCount)
                magazine.BulletCount = magazine.MaxBulletCount;

        }).Schedule();
    }
}