﻿using Assets;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public sealed class PistolShotSystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem _endSimulationEcbSystem;

    protected override void OnCreate()
    {
        _endSimulationEcbSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var bulletComponentGroup = GetComponentDataFromEntity<BulletComponent>();
        var localToWorldGroup = GetComponentDataFromEntity<LocalToWorld>();
        var ecb = _endSimulationEcbSystem.CreateCommandBuffer();
        var elapsedTime = (float) Time.ElapsedTime;

        Entities
            .WithAll<WeaponTriggerTag>()
            .WithBurst()
            .ForEach((ref PistolComponent pistolComponent,
                ref MagazineComponent magazine,
                in MouseInputComponent mouseInput,
                in Rotation rotation) =>
        {
            if (!mouseInput.LeftButtonIsDown || magazine.IsEmpty)
                return;

            var bulletEntity = ecb.Instantiate(pistolComponent.BulletPrefab);
            var bulletComponent = bulletComponentGroup[pistolComponent.BulletPrefab];
            var localToWorld = localToWorldGroup[pistolComponent.ShotPoint];

            var prefabComponent = new EntityPrefabReferenceComponent { Prefab = pistolComponent.BulletPrefab };
            var bulletTranslation = new Translation { Value = localToWorld.Position };
            var bulletRotation = new Rotation { Value = localToWorld.Rotation };
            var bulletInitData = new BulletInitialDataComponent
            {
                InitialPosition = localToWorld.Position,
                InitialVelocity = math.normalize(math.forward(rotation.Value)) * bulletComponent.Speed,
                InitialTime = elapsedTime,
                GravityFactor = 1.0f
            };

            ecb.SetComponent(bulletEntity, bulletTranslation);
            ecb.SetComponent(bulletEntity, bulletRotation);
            ecb.AddComponent(bulletEntity, prefabComponent);
            ecb.AddComponent(bulletEntity, bulletInitData);

            magazine.BulletCount--;

        }).Schedule();

        _endSimulationEcbSystem.AddJobHandleForProducer(Dependency);
    }
}
