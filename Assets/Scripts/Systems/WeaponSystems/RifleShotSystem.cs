using Assets;
using Unity.Entities;
using Unity.Transforms;

public sealed class RifleShotSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var elapsedTime = Time.ElapsedTime;

        Entities
            .WithAll<WeaponTriggerTag>()
            .WithBurst()
            .WithStructuralChanges()
            .ForEach((ref RifleComponent rifleComponent, ref MagazineComponent magazine, in MouseInputComponent mouseInput) =>
        {
            if (!mouseInput.LeftButtonIsPressed || magazine.IsEmpty)
                return;

            if (elapsedTime < rifleComponent.NextShotTime)
                return;

            var bulletEntity = EntityManager.Instantiate(rifleComponent.BulletPrefab);
            var localToWorld = EntityManager.GetComponentData<LocalToWorld>(rifleComponent.ShotPoint);

            var bulletTranslation = new Translation { Value = localToWorld.Position };
            var bulletRotation = new Rotation { Value = localToWorld.Rotation };

            EntityManager.SetComponentData(bulletEntity, bulletTranslation);
            EntityManager.SetComponentData(bulletEntity, bulletRotation);

            rifleComponent.NextShotTime = (float)elapsedTime + rifleComponent.RateOfFire;

            magazine.BulletCount--;

        }).Run();
    }
}
