﻿using Assets;
using Unity.Entities;
using Unity.Transforms;

public sealed class ShotgunShotSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities
            .WithAll<WeaponTriggerTag>()
            .WithBurst()
            .WithStructuralChanges()
            .ForEach((ref ShotgunComponent shotgunComponent,
                      ref MagazineComponent magazine,
                      in MouseInputComponent mouseInput,
                      in InputDataComponent keyboardInput) =>
        {
            if (!mouseInput.LeftButtonIsDown || magazine.IsEmpty)
                return;

            if (keyboardInput.IsRKeyDown && magazine.IsEmpty)
                shotgunComponent.IsRightDrummer = false;

            var bulletEntity = EntityManager.Instantiate(shotgunComponent.BulletPrefab);

            var shortPointEntity = shotgunComponent.IsRightDrummer
                ? shotgunComponent.LeftShotPoint
                : shotgunComponent.RightShotPoint;

            var localToWorld = EntityManager.GetComponentData<LocalToWorld>(shortPointEntity);

            var bulletTranslation = new Translation { Value = localToWorld.Position };
            var bulletRotation = new Rotation { Value = localToWorld.Rotation };

            EntityManager.SetComponentData(bulletEntity, bulletTranslation);
            EntityManager.SetComponentData(bulletEntity, bulletRotation);

            shotgunComponent.IsRightDrummer = !shotgunComponent.IsRightDrummer;

            magazine.BulletCount--;

        }).Run();
    }
}
