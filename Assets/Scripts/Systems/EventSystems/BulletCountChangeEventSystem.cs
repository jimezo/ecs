using Assets;
using System;
using Unity.Entities;

[AlwaysUpdateSystem]
[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class BulletCountChangeEventSystem : SystemBase
{
    public event Action<int> OnBulletEntityCount;

    private EntityQuery _bulletEntityQuery;
    private int _lastCalculatedBulletCount;

    protected override void OnCreate()
    {
        var bulletQuery = new EntityQueryDesc
        {
            All = new ComponentType[]
            {
                ComponentType.ReadOnly<BulletTag>()
            },

            None = new ComponentType[]
            {
                ComponentType.ReadOnly<Prefab>()
            }
        };

        _bulletEntityQuery = GetEntityQuery(bulletQuery);
    }

    protected override void OnUpdate()
    {
        var bulletCount = _bulletEntityQuery.CalculateEntityCount();

        if (bulletCount != _lastCalculatedBulletCount)
        {
            _lastCalculatedBulletCount = bulletCount;
            OnBulletEntityCount?.Invoke(bulletCount);
        }
    }
}
