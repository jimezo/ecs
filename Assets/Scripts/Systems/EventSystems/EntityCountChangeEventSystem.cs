using System;
using Unity.Entities;

[AlwaysUpdateSystem]
[UpdateInGroup(typeof(LateSimulationSystemGroup))]
public class EntityCountChangeEventSystem : SystemBase
{
    public event Action<int> OnNonPrefabEntityCount;

    private EntityQuery _nonPrefabEntityQuery;
    private int _lastCalculatedNonPrefabEntityCount;

    protected override void OnCreate()
    {
        var nonPrefabQuery = new EntityQueryDesc
        {
            None = new ComponentType[]
            {
                ComponentType.ReadOnly<Prefab>()
            }
        };

        _nonPrefabEntityQuery = GetEntityQuery(nonPrefabQuery);
    }

    protected override void OnUpdate()
    {
        var nonPrefabEntityCount = _nonPrefabEntityQuery.CalculateEntityCount();

        if (nonPrefabEntityCount != _lastCalculatedNonPrefabEntityCount)
        {
            _lastCalculatedNonPrefabEntityCount = nonPrefabEntityCount;
            OnNonPrefabEntityCount?.Invoke(nonPrefabEntityCount);
        }
    }
}
