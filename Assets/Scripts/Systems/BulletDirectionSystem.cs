﻿using Assets;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class BulletDirectionSystem : SystemBase
{
    private const float _gravityForce = -9.81f;

    protected override void OnUpdate()
    {
        var elapsedTime = (float) Time.ElapsedTime;

        //TODO : Temporary, needs to init once
        var gravityForce = new float3(0, _gravityForce, 0);

        Entities
            .WithBurst()
            .WithAll<BulletTag>()
            .ForEach((ref Translation translation, in BulletInitialDataComponent bulletInitialData) =>
            {
                var time = elapsedTime - bulletInitialData.InitialTime;
                var gravity = new float3(0, _gravityForce, 0);

                var velocity = bulletInitialData.InitialVelocity + gravity * time;
                var position = bulletInitialData.InitialPosition + velocity * time + gravity * time * time * 0.5f;
                translation.Value = position;

            }).ScheduleParallel();
    }
}
