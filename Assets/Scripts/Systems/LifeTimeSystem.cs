using Assets;
using Unity.Jobs;
using Unity.Entities;

public class LifeTimeSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem _endSimulationEcbSystem;

    protected override void OnCreate()
    {
        _endSimulationEcbSystem = World.GetExistingSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var elapsedTime = Time.ElapsedTime;
        var ecb = _endSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();

        Entities
            .WithNone<EntityDestroySystemStateComponent>()
            .ForEach((Entity entity, int entityInQueryIndex, ref LifeTimeComponent lifeTime) =>
            {

                if (lifeTime.DestroyTime == 0)
                    lifeTime.DestroyTime = (float) elapsedTime + lifeTime.LifeTime;

                if (elapsedTime >= lifeTime.DestroyTime)
                    ecb.AddComponent<EntityDestroySystemStateComponent>(entityInQueryIndex, entity);

            }).WithBurst().ScheduleParallel();

        _endSimulationEcbSystem.AddJobHandleForProducer(Dependency);
    }
}
