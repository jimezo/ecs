using UnityEngine;
using Unity.Entities;

public sealed class MouseInputSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var x = Input.GetAxisRaw("Mouse X");
        var y = Input.GetAxisRaw("Mouse Y");
        var leftButtonIsDown = Input.GetMouseButtonDown(0);
        var leftButtonIsPressed = Input.GetMouseButton(0);

        Entities.ForEach((ref MouseInputComponent mouseInputComponent) => {
            mouseInputComponent.MouseX = x;
            mouseInputComponent.MouseY = y;
            mouseInputComponent.LeftButtonIsDown = leftButtonIsDown;
            mouseInputComponent.LeftButtonIsPressed = leftButtonIsPressed;
        }).Schedule();
    }
}
