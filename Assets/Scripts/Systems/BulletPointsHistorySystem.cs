using Assets;
using Unity.Jobs;
using Unity.Entities;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;

[DisableAutoCreation]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateAfter(typeof(BulletDirectionSystem))]
public class BulletPointsHistorySystem : SystemBase
{
    public NativeArray<float3> Points;
    public int Count;

    protected override void OnStartRunning()
    {
        Points = new NativeArray<float3>(1000, Allocator.Persistent);
    }

    protected override void OnUpdate()
    {
        Entities
            .WithoutBurst()
            .WithAll<BulletTag>()
            .ForEach((in Translation translation) => 
        {
            Points[Count] = translation.Value;
            Count++;

        }).Run();
    }

    protected override void OnStopRunning()
    {
        Count = 0;
        Points.Dispose();
    }
}
