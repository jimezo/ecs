using Unity.Jobs;
using Unity.Entities;
using Unity.Transforms;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateAfter(typeof(BulletDirectionSystem))]
public class DrawBulletTrajectorySystem : SystemBase
{
    private EndSimulationEntityCommandBufferSystem _endSimulationEcbSystem;

    protected override void OnCreate()
    {
        _endSimulationEcbSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var ecb = _endSimulationEcbSystem.CreateCommandBuffer();

        Entities
            .ForEach((Entity entity, in Translation translation, in BulletComponent bulletComponent) => {

                var trajectotyEntity = ecb.Instantiate(bulletComponent.BulletTrajectoryPrefab);
                ecb.SetComponent(trajectotyEntity, new Translation() { Value = translation.Value });

        }).Schedule();

        _endSimulationEcbSystem.AddJobHandleForProducer(Dependency);
    }
}
