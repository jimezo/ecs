﻿using System;
using UnityEngine;

namespace Assets
{
    [Serializable]
    [CreateAssetMenu(fileName = "Rocket", menuName = "ScriptableObjects/Rocket")]
    public sealed class RocketScriptableObject : BulletAbstractScriptableObject
    {
        public RocketSpecification Specification;
    }
}
