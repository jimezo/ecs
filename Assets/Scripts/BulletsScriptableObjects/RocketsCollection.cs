﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Assets
{
    [Serializable]
    [CreateAssetMenu(fileName = "RocketsCollection", menuName = "ScriptableObjects/RocketsCollection")]
    public sealed class RocketsCollection : ScriptableObject
    {
        public List<RocketScriptableObject> Rockets;
    }
}
