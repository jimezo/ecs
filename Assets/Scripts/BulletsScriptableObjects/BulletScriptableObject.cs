﻿using System;
using UnityEngine;

namespace Assets
{
    [Serializable]
    [CreateAssetMenu(fileName = "Bullet", menuName = "ScriptableObjects/Bullet")]
    public sealed class BulletScriptableObject : BulletAbstractScriptableObject
    {
        public BulletSpecification Specification;
    }
}
