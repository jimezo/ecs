﻿using System;
using UnityEngine;
using Assets.Scripts.ECS;
using UnityEngine.AddressableAssets;

namespace Assets
{
    [Serializable]
    public abstract class BulletAbstractScriptableObject : ScriptableObject
    {
        public MongoId MongoId;
        public AssetReference BulletPrefab;
    }
}
