﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Assets
{
    [Serializable]
    [CreateAssetMenu(fileName = "BulletsCollection", menuName = "ScriptableObjects/BulletsCollection")]
    public sealed class BulletsCollection : ScriptableObject
    {
        public List<BulletScriptableObject> Bullets;
    }
}
