using System;
using Assets;
using UnityEditor;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Physics.Systems;
using System.Collections.Generic;
using Unity.Physics.GraphicsIntegration;
using Assets.Scripts.ExtensionMethods;

[CustomEditor(typeof(DrawBulletTrajectory))]
public class BulletEditor : Editor
{
    private Entity _bulletEntity;
    private World _world;
    private List<Vector3> _points = new List<Vector3>();
    private BulletPointsHistorySystem _bulletPointsHistorySystem;

    private void OnEnable()
    {
        if (EditorApplication.isPlaying)
            return;

        var targetObj = (DrawBulletTrajectory) target;

        if (targetObj == null)
            return;

        if (_world == null)
            _world = World.DefaultGameObjectInjectionWorld;

        var entityManager = _world.EntityManager;

        var bulletArchetype = entityManager.CreateArchetype(
            typeof(Translation),
            typeof(Rotation),
            typeof(LocalToWorld),
            typeof(BulletTag)
        );

        var trasnlation = new Translation { Value = targetObj.CannonPosition };
        var lifeTimeComponent = new LifeTimeComponent{ LifeTime = 1.5f };
        var rayCastComponent = new RaycastComponent { 
            Direction = new float3(0, 0, 1), 
            Distance = 1
        };

        var bulletInitDataComponent = new BulletInitialDataComponent
        {
            InitialPosition = targetObj.transform.position,
            InitialVelocity = math.normalize(targetObj.transform.forward) * targetObj.BulletSpeed,
            GravityFactor = 1.0f
        };

        _bulletEntity = entityManager.CreateEntity(bulletArchetype);

        entityManager.AddComponentData(_bulletEntity, new EntityPrefabReferenceComponent { Prefab = _bulletEntity });
        entityManager.AddComponentData(_bulletEntity, bulletInitDataComponent);
        entityManager.AddComponentData(_bulletEntity, rayCastComponent);
        entityManager.AddComponentData(_bulletEntity, lifeTimeComponent);
        entityManager.SetComponentData(_bulletEntity, trasnlation);

        if (_bulletPointsHistorySystem == null)
            _bulletPointsHistorySystem = _world.GetOrCreateSystem<BulletPointsHistorySystem>();

        var initGroup = _world.GetExistingSystem<InitializationSystemGroup>();
        var simulationSystemGroup = _world.GetExistingSystem<SimulationSystemGroup>();
        var fixedStepSimulationSystemGroup = _world.GetExistingSystem<FixedStepSimulationSystemGroup>();

        SystemExtensions.AddSystemsToGroup(_world, initGroup, new Type[]
        {
            typeof(UpdateWorldTimeSystem),
            typeof(ConvertToEntitySystem)
        });

        SystemExtensions.AddSystemsToGroup(_world, simulationSystemGroup, new Type[] {
            typeof(EntityDestoryStateSystem),
            typeof(BulletRicochetStateSystem),
            typeof(BulletPenetrationStateSystem),
            typeof(LifeTimeSystem),
        });

        SystemExtensions.AddSystemsToGroup(_world, fixedStepSimulationSystemGroup, new Type[] {
            typeof(BulletInitialTimeSetSystem),
            typeof(BulletDirectionSystem),
            typeof(BulletPointsHistorySystem),
            typeof(BuildPhysicsWorld),
            typeof(StepPhysicsWorld),
            typeof(BulletRayCastSystem),
            typeof(RecordMostRecentFixedTime),
            typeof(ExportPhysicsWorld),
            typeof(EndFramePhysicsSystem)
        });

        EditorApplication.update += OnWorldUpdate;
    }

    private void OnSceneGUI()
    {
        if (EditorApplication.isPlaying || _world == null)
            return;

        if (_points.Count == 0)
            return;

        Handles.DrawPolyLine(_points.ToArray());
    }

    private void OnWorldUpdate()
    {
        var entityManager = _world.EntityManager;

        if (entityManager.Exists(_bulletEntity) && entityManager.HasComponent<EntityDestroySystemStateComponent>(_bulletEntity))
        {
            var entityDestroyState = entityManager.GetComponentData<EntityDestroySystemStateComponent>(_bulletEntity);

            if (entityDestroyState.IsDestroyed)
            {
                for (var i = 0; i < _bulletPointsHistorySystem.Count; i++)
                    _points.Add(_bulletPointsHistorySystem.Points[i]);

                entityManager.RemoveComponent<EntityDestroySystemStateComponent>(_bulletEntity);
            }
        }

        _world.Update();
    }

    private void OnDisable()
    {
        _points.Clear();
        EditorApplication.update -= OnWorldUpdate;
    }
}