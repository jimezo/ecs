using UnityEditor;
using Unity.Mathematics;

[CustomEditor(typeof(AngleOfVectors))]
public class AngleOfVectorsEditor : Editor
{
    private float3 _objPosition;
    private float3 _firstVector;
    private float3 _secondVector;
    private AngleOfVectors _targetObj;

    private void OnEnable()
    {
        _targetObj = target as AngleOfVectors;
    }

    private void OnSceneGUI()
    {
        if (_targetObj == null)
            return;

        _objPosition = new float3(_targetObj.transform.position);
        _firstVector = _targetObj.FirstVectorInfo.Position;
        _secondVector = _targetObj.SecondVectorInfo.Position;

        var dotProduct = math.dot(_firstVector, _secondVector);
        var firstVectorMagnitude = math.pow(2, _firstVector);
        var secondVectorMagnitude = math.pow(2, _secondVector);
        var result = dotProduct / (firstVectorMagnitude * secondVectorMagnitude);

        _targetObj.Angle = math.acos(result) * 180 / math.PI;

        Handles.color = _targetObj.FirstVectorInfo.Color;
        Handles.DrawLine(_objPosition, _firstVector + _objPosition);

        Handles.color = _targetObj.SecondVectorInfo.Color;
        Handles.DrawLine(_objPosition, _secondVector + _objPosition);
    }
}
