﻿using System;
using System.Linq;
using Unity.Entities;
using Assets.Scripts.ECS;
using System.Collections.Generic;

namespace Assets
{
    public sealed class BulletsPool : IBulletsPool
    {
        private readonly Dictionary<MongoId, Stack<Entity>> _id2EntitiesPool = new Dictionary<MongoId, Stack<Entity>>();
        private readonly EntityFactoryDelegate _factoryDelegate;

        public BulletsPool(EntityFactoryDelegate factoryDelegate)
        {
            _factoryDelegate = factoryDelegate;
        }

        public void Push(Entity entity)
        {
            throw new NotImplementedException();
        }

        public Entity Pop(MongoId mongoId)
        {
            if (_id2EntitiesPool.TryGetValue(mongoId, out var entityPool) && entityPool.Any())
                return entityPool.Pop();

            return _factoryDelegate(mongoId);
        }
    }
}
