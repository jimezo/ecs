﻿using Unity.Entities;
using Assets.Scripts.ECS;

namespace Assets
{
    public interface IBulletsPool
    {
        void Push(Entity entity);
        Entity Pop(MongoId mongoId);
    }
}
