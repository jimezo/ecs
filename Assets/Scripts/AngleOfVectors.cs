using System;
using UnityEngine;
using Unity.Mathematics;

public class AngleOfVectors : MonoBehaviour
{
    public VectorInfo FirstVectorInfo;
    public VectorInfo SecondVectorInfo;
    public float3 Angle;
}

[Serializable]
public struct VectorInfo
{
    public float3 Position;
    public Color Color;
} 
