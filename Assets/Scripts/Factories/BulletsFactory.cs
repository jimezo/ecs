﻿using System;
using System.Linq;
using UnityEngine;
using Unity.Entities;
using Assets.Scripts.ECS;
using UnityEngine.AddressableAssets;

namespace Assets
{
    public delegate Entity EntityFactoryDelegate(MongoId mongoId);

    public class BulletsFactory
    {
        private readonly BulletsCollection _bulletsCollection;
        private readonly RocketsCollection _rocketsCollection;
        private readonly World _world;

        public BulletsFactory(World world, BulletsCollection bulletsCollection, RocketsCollection rocketsCollection)
        {
            _bulletsCollection = bulletsCollection;
            _rocketsCollection = rocketsCollection;
            _world = world;
        }

        public Entity CreateBullet(MongoId mongoId)
        {
            var bulletObject = _bulletsCollection.Bullets.FirstOrDefault(bullet => bullet.MongoId.Value == mongoId.Value);

            if (bulletObject == null)
                throw new ArgumentException();

            var bulletPrefab = Addressables.InstantiateAsync(bulletObject.BulletPrefab).Result;

            return CreateBulletEntity(bulletPrefab);
        }

        public Entity CreateRocket(MongoId mongoId)
        {
            var rocketObject = _rocketsCollection.Rockets.FirstOrDefault(rocket => rocket.MongoId.Value == mongoId.Value);

            if (rocketObject == null)
                throw new ArgumentException();

            var rocketPrefab = Addressables.InstantiateAsync(rocketObject.BulletPrefab).Result;

            return CreateBulletEntity(rocketPrefab);
        }

        private Entity CreateBulletEntity(GameObject gameObject)
        {
            var components = gameObject.GetComponents<ComponentType>();
            return _world.EntityManager.CreateEntity(components);
        }
    }
}
