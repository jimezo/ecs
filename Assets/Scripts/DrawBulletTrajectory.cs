using UnityEngine;
using Unity.Mathematics;

public class DrawBulletTrajectory : MonoBehaviour
{
    //TODO : Temporary
    public float BulletSpeed;

    [SerializeField] private GameObject Cannon;
    [HideInInspector] public float3 CannonPosition => Cannon.transform.position;
}