﻿using Unity.Entities;

namespace Assets 
{
    [GenerateAuthoringComponent]
    public sealed class RocketComponent : IComponentData
    {
        public Entity VisualPrefab;
        public RocketScriptableObject ScriptableObject;
    }
}

