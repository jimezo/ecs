﻿using Unity.Entities;

namespace Assets
{
    [GenerateAuthoringComponent]
    public sealed class PatronComponent : IComponentData
    {
        public Entity VisualPrefab;
        public BulletScriptableObject ScriptableObject;
    }
}

