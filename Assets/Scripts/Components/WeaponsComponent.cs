using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct WeaponsComponent : IComponentData
{
    [HideInInspector] 
    public Entity CurrentWeapon;

    public Entity FirstWeapon;
    public Entity SecondWeapon;
    public Entity ThirdWeapon;
}
