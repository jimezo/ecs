using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct LifeTimeComponent : IComponentData
{
    [HideInInspector]
    public float DestroyTime;

    public float LifeTime;
}
