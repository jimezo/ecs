﻿using MongoDB.Bson;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.ECS
{
    [Serializable]
    [GenerateAuthoringComponent]
    public struct MongoId : IComponentData
    {
        [HideInInspector]
        public ObjectId ObjectId;

        [OdinSerialize]
        [ShowInInspector]
        public string Value
        {
            get => ObjectId.ToString();
            set => ObjectId = ObjectId.Parse(value);
        }
    }
}