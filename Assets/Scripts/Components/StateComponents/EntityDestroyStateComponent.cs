using UnityEngine;
using Unity.Entities;

namespace Assets
{
    [GenerateAuthoringComponent]
    public struct EntityDestroySystemStateComponent : ISystemStateComponentData
    {
        [HideInInspector] public bool IsDestroyed;
    }
}

