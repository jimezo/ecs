using System;
using Unity.Physics;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

namespace Assets
{
    [Serializable]
    public struct BulletCollisionStateComponent : ISystemStateComponentData
    {
        public Entity BulletPrefab;
        public float3 InitialVelocity;
        public Translation Translation;
        public Rotation Rotation;
        public RaycastHit RaycastHit;
    }
}
