using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct RifleComponent : IComponentData
{
    [HideInInspector]
    public float NextShotTime;

    public Entity BulletPrefab;
    public Entity ShotPoint;
    public float RateOfFire;
}
