﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct ShotgunComponent : IComponentData
{
    [HideInInspector]
    public bool IsRightDrummer;

    public Entity BulletPrefab;
    public Entity LeftShotPoint;
    public Entity RightShotPoint;
}
