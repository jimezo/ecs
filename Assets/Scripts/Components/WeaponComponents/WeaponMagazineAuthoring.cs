﻿using UnityEngine;
using Unity.Entities;

public struct MagazineComponent : IComponentData
{
    [HideInInspector]
    public int BulletCount;

    [HideInInspector]
    public bool IsEmpty;

    public int MaxBulletCount;
}

public class WeaponMagazineAuthoring : MonoBehaviour, IConvertGameObjectToEntity
{
    public int MaxBulletCount;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponentData(entity, new MagazineComponent()
        {
            MaxBulletCount = this.MaxBulletCount,
            BulletCount = this.MaxBulletCount,
            IsEmpty = this.MaxBulletCount == 0
        });
    }
}
