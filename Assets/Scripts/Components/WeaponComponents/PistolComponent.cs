﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct PistolComponent : IComponentData
{
    public Entity BulletPrefab;
    public Entity ShotPoint;
}
