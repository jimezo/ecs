using Unity.Entities;

namespace Assets
{
    [GenerateAuthoringComponent]
    public struct EntityPrefabReferenceComponent : IComponentData
    {
        public Entity Prefab;
    }
}
