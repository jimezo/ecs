﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct BulletComponent : IComponentData
{
    public Entity BulletTrajectoryPrefab;

    public float Speed;
    public float CaliberLength;
    public float ProjectileMass;

    [HideInInspector] public float3 Velocity;
}
