using System;
using Unity.Entities;

[Serializable]
public struct BulletStateSharedComponent : ISharedComponentData
{
    public EBulletCollisionState State;
}

public enum EBulletCollisionState
{
    Collision,
    Ricochet,
    Penetration
}
