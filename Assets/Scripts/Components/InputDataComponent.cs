﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct InputDataComponent : IComponentData
{
    [HideInInspector] public bool IsAlpha1;
    [HideInInspector] public bool IsAlpha2;
    [HideInInspector] public bool IsAlpha3;

    [HideInInspector] public bool IsJumpInput;

    [HideInInspector] public float InputH;
    [HideInInspector] public float InputV;
    [HideInInspector] public bool IsRKeyDown;
}


