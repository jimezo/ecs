﻿using Unity.Entities;
using Unity.Mathematics;

namespace Assets
{
    public struct CannonComponent : IComponentData
    {
        public Entity BulletPrefab;
    }
}
