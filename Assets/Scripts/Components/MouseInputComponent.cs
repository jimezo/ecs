﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct MouseInputComponent : IComponentData
{
    [HideInInspector] public float MouseX;
    [HideInInspector] public float MouseY;
    [HideInInspector] public bool LeftButtonIsDown;
    [HideInInspector] public bool LeftButtonIsPressed;
}