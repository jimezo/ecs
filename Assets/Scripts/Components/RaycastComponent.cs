﻿using System;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
[GenerateAuthoringComponent]
public struct RaycastComponent : IComponentData
{
    public float Distance;
    public float3 Direction;
}
