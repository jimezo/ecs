using Unity.Entities;

namespace Assets
{
    [GenerateAuthoringComponent]
    public struct WeaponTriggerTag : IComponentData {}
}

