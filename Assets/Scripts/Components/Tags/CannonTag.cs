﻿using Unity.Entities;

namespace Assets
{
    [GenerateAuthoringComponent]
    public struct CannonTag : IComponentData {}
}
