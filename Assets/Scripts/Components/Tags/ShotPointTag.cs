﻿using Unity.Entities;

namespace Assets
{
    [GenerateAuthoringComponent]
    public struct ShotPointTag : IComponentData {}
}
