﻿using Unity.Entities;

namespace Assets
{
    [GenerateAuthoringComponent]
    public struct BulletTag : IComponentData {}
}
