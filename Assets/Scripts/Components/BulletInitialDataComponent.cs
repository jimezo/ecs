using System;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
[GenerateAuthoringComponent]
public struct BulletInitialDataComponent : IComponentData
{
    [HideInInspector] public float3 InitialPosition;
    [HideInInspector] public float3 InitialVelocity;
    [HideInInspector] public float InitialTime;
    [HideInInspector] public float GravityFactor;
}
